/**
 * @jest-environment jsdom
 */

const { TextEncoder, TextDecoder } = require('util'); 
global.TextEncoder = TextEncoder;
global.TextDecoder = TextDecoder;

const { JSDOM } = require('jsdom');
const fs = require('fs');
const path = require('path');

let document;
let window;

beforeEach(() => {
    // Charger le fichier HTML
    const html = fs.readFileSync(path.resolve(__dirname, '../frontend/public/index.html'), 'utf8');
    const dom = new JSDOM(html, { runScripts: 'dangerously' });
    document = dom.window.document;
    window = dom.window;
});

test('Les champs du formulaire doivent être remplis', () => {
    // Remplir les champs du formulaire
    document.getElementById('nom').value = 'John';
    document.getElementById('prenom').value = 'Doe';
    document.getElementById('telephone').value = '1234567890';
    document.getElementById('email').value = 'john.doe@example.com';
    document.getElementById('formation').value = 'Formation XYZ';

    // Vérifier que les champs ne sont pas vides
    expect(document.getElementById('nom').value).toBeTruthy();
    expect(document.getElementById('prenom').value).toBeTruthy();
    expect(document.getElementById('telephone').value).toBeTruthy();
    expect(document.getElementById('email').value).toBeTruthy();
    expect(document.getElementById('formation').value).toBeTruthy();
});

test('Les champs nom, prénom et telephone ne doivent pas contenir de caractères spéciaux', () => {
    // Remplir les champs du formulaire
    document.getElementById('nom').value = 'John';
    document.getElementById('prenom').value = 'Doe';
    document.getElementById('telephone').value = '1234567890';

    // Vérifier qu'il n'y a pas de caractères spéciaux dans le champ nom
    expect(/[^\w\s]/.test(document.getElementById('nom').value)).toBeFalsy();

    // Vérifier qu'il n'y a pas de caractères spéciaux dans le champ prénom
    expect(/[^\w\s]/.test(document.getElementById('prenom').value)).toBeFalsy();

    // Vérifier qu'il n'y a pas de caractères spéciaux dans le champ téléphone
    expect(/[^\d]/.test(document.getElementById('telephone').value)).toBeFalsy();
});

test("L'email doit correspondre au format attendu", () => {
    // Remplir le champ email avec une valeur valide
    document.getElementById('email').value = 'john.doe@example.com';

    // Vérifier que l'email correspond au format attendu
    const email = document.getElementById('email').value;
    const isValidEmail = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/.test(email);

    expect(isValidEmail).toBeTruthy();
});
