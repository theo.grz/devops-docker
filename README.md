# Automatisation du Déploiement avec Docker et GitLab CI/CD

## Introduction
Ce document explique en détail les étapes nécessaires pour automatiser le déploiement d'une application web en utilisant Docker et GitLab CI/CD. Nous nous concentrerons sur la configuration des fichiers Docker, la mise en place de pipelines CI/CD, et l'utilisation de Docker Hub pour stocker et gérer les images Docker.

## Objectifs
1. **Automatisation du Déploiement :** Mettre en place un processus automatisé pour déployer des applications web.
2. **Gestion des Conteneurs avec Docker :** Utiliser Docker pour conteneuriser les applications frontend et backend.
3. **Configuration des Pipelines CI/CD :** Utiliser GitLab CI/CD pour configurer et gérer les pipelines de build et de déploiement.

## Prérequis
- Un compte Docker Hub.
- Un projet GitLab avec GitLab CI/CD activé.
- Une VM (Machine Virtuelle) pour exécuter les conteneurs Docker.

## Étapes de Réalisation

1. **Création des Dockerfiles:** Les Dockerfiles pour le frontend et le backend sont déjà présents dans le dépôt. Ils sont utilisés pour créer les images Docker lors de l'exécution du pipeline CI/CD.

2. **Configuration du Fichier .gitlab-ci.yml:** Le fichier .gitlab-ci.yml est déjà configuré dans ce projet pour définir les différentes étapes du pipeline CI/CD, telles que la construction des images Docker, les tests et le déploiement.

3. **Création des Repositories Docker Hub:**
    - Connectez-vous à Docker Hub.
    - Créez deux repositories : frontend et backend.

4. **Configuration des Variables GitLab CI/CD:**
    - Allez dans votre projet GitLab.
    - Accédez aux Settings > CI/CD > Variables.
    - Ajoutez les variables suivantes :
        - `CI_REGISTRY_USER` : votre nom d'utilisateur Docker Hub.
        - `CI_REGISTRY_PASSWORD` : votre mot de passe Docker Hub.

5. **Exécution du Pipeline CI/CD:**
    Une fois que tout est configuré, poussez vos modifications vers le dépôt GitLab. Le pipeline CI/CD se déclenchera automatiquement, construisant et poussant les images Docker vers Docker Hub.

6. **Récupération et Exécution des Conteneurs sur la VM:**
    *Nous utiliserons le projet docker hub lié à theo930. Si vous avez un autre repository docker hub, changez `theo930` par le vôtre.*
    - Connectez-vous à votre VM.
    - Connectez-vous à Docker Hub :
        ```
        docker login
        ```
    - Récupérez les images Docker :
        ```
        docker pull theo930/backend:latest
        docker pull theo930/frontend:latest
        ```
    - Exécutez les conteneurs :
        ```
        docker run -d -p 8080:80 --name backend-container theo930/backend:latest
        docker run -d -p 3000:3000 --name frontend-container theo930/frontend:latest
        ```

## Commandes Utiles pour Docker
- Lister les conteneurs en cours d'exécution :
    ```
    docker ps
    ```
- Arrêter un conteneur :
    ```
    docker stop [CONTAINER_ID]
    ```
- Supprimer un conteneur :
    ```
    docker rm [CONTAINER_ID]
    ```
- Afficher les logs d'un conteneur :
    ```
    docker logs [CONTAINER_NAME]
    ```
- Se connecter à un conteneur en cours d'exécution :
    ```
    docker exec -it [CONTAINER_NAME] /bin/bash
    ```
